import './App.css';
import Logo from './components/logo'
import Subtitle from './components/subtitle'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Logo />
        <Subtitle
          username="Angie"
          chapter="8"
        />
      </header>
    </div>
  );
}

export default App;
