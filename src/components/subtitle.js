import React, { Component, Fragment } from 'react'
import Chapter from './chapter'

class Subtitle extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: 'Learn Reactjs',
            currentChapter: '7'
        };
        this.changeTitle = this.changeTitle.bind(this)
    }
        changeTitle(){
            this.setState({
                title: 'Belajar Reactjs'
            })
        }

    render() {
        const { username } = this.props
        const { title, currentChapter } = this.state
    
        return (
            <Fragment>
                <p>{title}</p>
                <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer" > Hello {username} </a>
                <Chapter chapter = {currentChapter}/>
                <button onClick={this.changeTitle}>Ubah Judul</button>
            </Fragment>
       )
    }
}

export default Subtitle;