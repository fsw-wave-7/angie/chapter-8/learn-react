import logo from '../logo.svg';
import React, { Component } from 'react'

class Logo extends Component {
    render() {
     return < img src = {
          logo
      }
      className = "App-logo"
      alt = "logo" />
    }
}

export default Logo;